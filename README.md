# README #

### What is this repository for? ###

I got tired of reformatting my resume for this gig or that so I wrote this is a AngularJS single page application that generates a tailored resume.  Please feel free to fork it and put in your own data.  See below on how to do that.

### How do I get set up? ###

* Fork or download it
* Open index.html
* Click skills to enable or disable them and update the experience displayed below
* To add your own details, edit service.resume.js and put your data into myResume

### Contribution guidelines ###

* Any pull requests must have no JS hint warnings or errors unless you can't avoid JS hint warnings.  Please explain in a comment why you can't avoid the warning.

### Who do I talk to? ###

* Repo owner or admin